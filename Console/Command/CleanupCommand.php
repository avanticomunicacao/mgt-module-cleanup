<?php
namespace Avanti\Cleanup\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;

class CleanupCommand extends Command
{
    const ROOT_CATEGORY = 1;
    const DEFAULT_CATEGORY = 2;

    private $searchCriteria;
    private $productRepository;
    private $logger;
    private $state;
    private $categoryList;
    private $categoryRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaInterface $searchCriteria,
        LoggerInterface $logger,
        State $state,
        CategoryListInterface $categoryList,
        CategoryRepositoryInterface $categoryRepository,
        $name = null
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteria = $searchCriteria;
        $this->logger = $logger;
        $this->state = $state;
        $this->categoryList = $categoryList;
        $this->categoryRepository = $categoryRepository;
        parent::__construct($name);
    }


    protected function configure()
    {
        $this->setName("avanti:cleanup");
        $this->setDescription('Clean the products and categories.');
        $this->addOption(
            "products",
            "p",
            InputOption::VALUE_NONE,
            "Remove all products from the database"
        );
        $this->addOption(
            "categories",
            "c",
            InputOption::VALUE_NONE,
            "Remove all categories from the database"
        );
        $this->addOption(
            'all',
            'a',
            InputOption::VALUE_NONE,
            'Remove all products and categories from the database'
        );

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(Area::AREA_ADMINHTML);
        if ($input->getOption('products')) {
            $this->removeProducts();
            $output->writeln('<info>Products Success Removed.</info>');
        }

        if ($input->getOption('categories')) {
            $this->removeCategories();
            $output->writeln('<info>Categories Success Removed.</info>');
        }

        if ($input->getOption('all')) {
            $this->removeProducts();
            $this->removeCategories();
        }
    }

    protected function removeProducts()
    {
        try {
            $search = $this->searchCriteria->setPageSize(0);
            $products = $this->productRepository->getList($search)->getItems();
            foreach($products as $product) {
                try {
                    $p = $this->productRepository->getById($product->getId());
                    $this->productRepository->delete($p);
                } catch (\Exception $e) {
                    $this->logger->error('Product Remove Error. Error Log:');
                    $this->logger->error($e);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Avanti Cleanup Product Error. Error Log:');
            $this->logger->error($e);
        }
    }

    protected function removeCategories()
    {
        try {
            $search = $this->searchCriteria->setPageSize(0);
            $categories = $this->categoryList->getList($search)->getItems();
            foreach ($categories as $category) {
                if ($category->getId() != self::ROOT_CATEGORY && $category->getId() != self::DEFAULT_CATEGORY) {
                    try {
                        $c = $this->categoryRepository->get($category->getId());
                        $this->categoryRepository->delete($c);
                    } catch (\Exception $e) {
                        $this->logger->error('Category Remove Error. Error Log:');
                        $this->logger->error($e);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Avanti Cleanup Category Error. Error Log:');
            $this->logger->error($e);
        }
    }
}
