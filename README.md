## Module Cleanup
### Funcionalidades
Com esse módulo, é possível zerar a árvore de categorias e os produtos de uma loja Magento 2.

### Uso
As funcionalidades desse módulo são acessadas através do magento-cli. Utilize o seguinte código
para exibir as opções do módulo:

``php bin/magento avanti:cleanup --help``

fazendo isso será exibida na tela as opções disponíveis, e como utilizá-las. Conforme imagem abaixo:
![Image of Help](docs/info.png)

Teremos 3 opções disponíveis que podemos utilizar:
* --products = para deletar todos os produtos
* --categories = para deletar todas as categorias
* --all = para deletar tanto os produtos quando as categorias

então para utilizar por exemplo a opção de excluir todos os produtos, basta digitar o seguinte comando:

``php bin/magento avanti:cleanup --products``

ou digitar o seguinte:

``php bin/magento avanti:cleanup -p``
